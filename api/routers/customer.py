from fastapi import APIRouter,Depends,Response
from typing import Union
from queries.customer import CustomerRepository, CustomerIn,CustomerOut,ListCostumer

router=APIRouter()

@router.post('/Customer/create', response_model=CustomerOut)
def create(
    customer_in:CustomerIn,
    repo:CustomerRepository=Depends()
):
   return repo.create(customer_in)
@router.get('/Customer/get_all', response_model=ListCostumer)
def create(
    repo:CustomerRepository=Depends()
):
   return repo.get_all_customer()
