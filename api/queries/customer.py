from pydantic import BaseModel
from typing import Optional, List,Union
from datetime import date
from queries.pool import pool

class CustomerIn(BaseModel):
    name:str
    lastName:str
class CustomerOut(BaseModel):
    name:str
    lastName:str
    id:int
class ListCostumer(BaseModel):
    costumers:List[CustomerOut]


class CustomerRepository:
    def create(self,customer_in:CustomerIn):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result=db.execute(
                    '''
                    INSERT INTO costumer(
                    name, lastName)
                    VALUES (%s, %s)
                    RETURNING ID
                    ''',
                    [
                        customer_in.name,
                        customer_in.lastName
                    ]
                )
                old_data=customer_in.dict()
                id=result.fetchone()[0]
                return CustomerOut(id=id,**old_data)

    def costumer_into_lis(self,content):
        lis=[]
        for i in content:
            dic={'id':None,'name':None,'lastName':None}
            dic['id']=i[0]
            dic['name']=i[1]
            dic['lastName']=i[2]
            lis.append(dic)
        return lis

    def get_all_customer(self,):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result=db.execute(
                    '''
                    SELECT * FROM costumer;
                    ''',
                )
                list_customer=self.costumer_into_lis(content=result.fetchall())
                return ListCostumer(costumers=list_customer)
