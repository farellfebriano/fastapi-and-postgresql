from fastapi import FastAPI
#vacations is the file so baicaly we import all the file
from routers import vacations, customer
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
origin=[
    'http://localhost:3000'
]
# add middle ware
app.add_middleware(
    CORSMiddleware,
    allow_origins=origin,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)
#this is how to hook the router in to the main router
app.include_router(vacations.router,tags=['vacations'])
app.include_router(customer.router,tags=['customer'])
