import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Nav from './Nav';
import CreateCostumers from './CreateCostumers';
import React  from 'react';

function App() {
  return (
    <BrowserRouter>
     <Nav />
      <div className='container'>
        <Routes>
          <Route path='consumer/create' element={<CreateCostumers />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
