import { NavLink } from 'react-router-dom';
import { useState,useEffect } from 'react';
import React  from 'react';

function Nav() {
    const [consumers,setConsumer]=useState([])
    const consumer = async ()=>{
        const url='http://localhost:8000/Customer/get_all'
        const response= await fetch(url)
        if ( response.ok){
            console.log('success ... ... ')
            // const content=await response.json()
            const content=await response.json()
            setConsumer(content.costumers.length)
        }
    }
    const [vacations,setVacation]=useState([])
    const vacation = async ()=>{
        const url='http://localhost:8000/vacations'
        const response= await fetch(url)
        if ( response.ok){
            console.log('success ... ... ')
            const content=await response.json()
            setVacation(content.length)
        }
    }
    console.log();
    useEffect(()=>{
        consumer()
        vacation()
    },[])
    console.log();
    // console.log(vacations,);

    return (
<>
<nav className="navbar navbar-expand-lg bg-body-tertiary">
  <div className="container-fluid">
    <NavLink className="navbar-brand" href="#">Navbar</NavLink>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className="navbar-nav">
        <li className="nav-item">
          <NavLink className="nav-link active" aria-current="page" href="#">consumer {consumers}</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" href="#">vacations {vacations}</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="consumer/create">CreateCostumers </NavLink>
        </li>
      </ul>
    </div>
  </div>
</nav>
</>
  )
}
export default Nav;
