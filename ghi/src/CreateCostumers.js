import { useState} from "react";
import React  from 'react';
function CreateCostumers(){
    const[name,setname]=useState('')
    const nameHandler=(event)=>{
        const value= event.target.value
        setname(value)
    }
    const[lastName,setLastName]=useState('')
    const lastNameHandler=(event)=>{
        const value= event.target.value
        setLastName(value)
    }
    const submitHandler=async(event)=>{
        event.preventDefault()
        const dic={}
        dic.name=name
        dic.lastName=lastName
        const content=JSON.stringify(dic)
        const url='http://localhost:8000/Customer/create'
        const fetchConfig = {
            method: 'post',
            body: content,
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response= await fetch(url,fetchConfig)
        if(response.ok){
            console.log('success posting data .... ... ...');
            setname('')
            setLastName('')


        }else{
            console.log('fail .... ... ...');
        }

    }
    return (
<>
<div className="row">
        <div className='offset-3 col-6'>
            <div className="shadow p-4 mt-4">
                <h1>Create Consumer</h1>
                    <form  onSubmit={submitHandler} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input  value={name} onChange={nameHandler} type="text" className="form-control" id="floatingInput" placeholder="name@example.com" />
                            <label htmlFor="floatingInput">name</label>
                        </div>
                        <div className="form-floating">
                            <input value={lastName}  onChange={lastNameHandler} type="text" className="form-control" id="floatingPassword" placeholder="Password" />
                            <label htmlFor="floatingPassword">last name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
</div>
        </div>
            </div>

</>
)
}

export default CreateCostumers
